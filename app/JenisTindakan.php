<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
class JenisTindakan extends Model
{
    protected $table="m_jenis_tindakan";
    protected $primaryKey="m_jenis_tindakan_id";
    protected $fillable=["active", "create_date", "update_date","nama"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

public static function get_list_jenistindakan(){
        $sql="SELECT *
          FROM m_jenis_tindakan
                WHERE m_jenis_tindakan.active=1";
        $data=DB::connection()->select($sql);
        return $data;
    }
    
}
