<?php

namespace App\Http\Controllers;

use App\JenisTindakan;
use App\UsersMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class JenisTindakanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.master.m_jenistindakan.index", compact("permission"));
        }

    }

    public function data(){
        $jenistindakan=JenisTindakan::get_list_jenistindakan();
        $permission=UsersMenu::getPermission(3);
        return DataTables::of($jenistindakan)

            ->addColumn("action", function ($jenistindakan){
                $permission=UsersMenu::getPermission(3);
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("jenistindakan.edit", base64_encode($jenistindakan->m_jenis_tindakan_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($jenistindakan->m_jenis_tindakan_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
           
          
            return view("adminlte.master.m_jenistindakan._form", compact("permission"));
        }
    }

    public function save(Request $request){
        $this->validate($request,[
            "nama"=>"required",

        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",

        ]);

        try{

            $jenistindakan=new JenisTindakan();
            $jenistindakan->nama=ucwords($request->get("nama"));
            $jenistindakan->save();
            return redirect()->route("jenistindakan.list");
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $jenistindakan=JenisTindakan::findOrFail(base64_decode($id));
            $action="Edit";
            
            return view("adminlte.master.m_jenistindakan._form", compact("permission", "action", "jenistindakan"));
        }
    }

    public function update(Request $request){
        $this->validate($request,[
           "nama"=>"required",
          
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
   
        ]);
        try{

              $jenistindakan=JenisTindakan::findOrFail($request->get("m_jenis_tindakan_id"));
            $arrUpdate=array(
                "nama"=>ucwords($request->get("nama")),
    
            );
            $jenistindakan->update($arrUpdate);
            return redirect()->route("jenistindakan.list");

        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function delete(Request $request){
        $permission=UsersMenu::getPermission(3);
        if($permission[0]->_delete==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            try{
                $jenistindakan=JenisTindakan::findOrFail(base64_decode($request->get("tmpId")));
                $jenistindakan->update(array("active"=>0));
                return redirect()->route("jenistindakan.list");
            }
            catch (\Exception $e){
                $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
                $title="Error";
                return view("adminlte.error_page",compact("message", "title"));
            }
        }
    }
}