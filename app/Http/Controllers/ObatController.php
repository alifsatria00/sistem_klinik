<?php

namespace App\Http\Controllers;

use App\Obat;
use App\UsersMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class ObatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(4);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.master.m_obat.index", compact("permission"));
        }

    }

    public function data(){
        $obat=Obat::get_list_obat();
        $permission=UsersMenu::getPermission(4);
        return DataTables::of($obat)

            ->addColumn("action", function ($obat){
                $permission=UsersMenu::getPermission(2);
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("obat.edit", base64_encode($obat->m_obat_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($obat->m_obat_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
           
            return view("adminlte.master.m_obat._form", compact("permission"));
        }
    }

    public function save(Request $request){
        $this->validate($request,[
            "nama"=>"required",

        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",

        ]);

        try{

            $obat=new Obat();
            $obat->nama=ucwords($request->get("nama"));
             $obat->harga=ucwords($request->get("harga"));
            $obat->save();
            return redirect()->route("obat.list");
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $obat=Obat::findOrFail(base64_decode($id));
            $action="Edit";
            
            return view("adminlte.master.m_obat._form", compact("permission", "action", "obat"));
        }
    }

    public function update(Request $request){
        $this->validate($request,[
           "nama"=>"required",
          
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
   
        ]);
        try{

              $obat=Obat::findOrFail($request->get("m_obat_id"));
            $arrUpdate=array(
                "nama"=>ucwords($request->get("nama")),
                 "harga"=>ucwords($request->get("harga")),
    
            );
            $obat->update($arrUpdate);
            return redirect()->route("obat.list");

        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function delete(Request $request){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_delete==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            try{
                $obat=Obat::findOrFail(base64_decode($request->get("tmpId")));
                $obat->update(array("active"=>0));
                return redirect()->route("obat.list");
            }
            catch (\Exception $e){
                $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
                $title="Error";
                return view("adminlte.error_page",compact("message", "title"));
            }
        }
    }
}