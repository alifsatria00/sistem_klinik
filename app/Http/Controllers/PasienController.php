<?php

namespace App\Http\Controllers;


use App\MLokasi;
use App\Pasien;
use App\PasienDetail;
use App\Obat;
use App\User;
use App\UsersMenu;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class PasienController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(5);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.transaksi.t_pasien.index", compact("permission"));
        }

    }

    public function data(){
    	$pasien=Pasien::get_list_t_pasien();
    	$permission=UsersMenu::getPermission(5);
        return DataTables::of($pasien)
              ->addColumn("create_date", function($pasien) {
                return date('d-F-Y', strtotime($pasien->create_date));
            })
              ->addColumn("action", function ($pasien) use ($permission){
                $editable="";
                $deletable="";
                $ubahStatus="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                if($pasien->status>0){
                    $editable="disabled='disabled' onclick='return false;'";
                    $deletable="disabled='disabled' onclick='return false;'";
                }

                if($pasien->status==0){
                    //$ongkir="disabled='disabled' onclick='return false;'";
                    $ubahStatus="disabled='disabled' onclick='return false;'";
                }
                elseif($pasien->status==1){
                     $editable="";
                    $ubahStatus="";
                    $deletable="";
                }
                  elseif($pasien->status==2){
                    $ubahStatus="disabled='disabled' onclick='return false;'";
                    $deletable="";
                     $editable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("pasien.edit", base64_encode($pasien->t_pasien_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a>
                        <a href=\"javascript:showConfirm2('".base64_encode($pasien->t_pasien_id)."')\" class='btn btn-sm btn-success' data-toggle='tooltip' data-placement='bottom' title='Konfirmasi Pembayaran' ".$ubahStatus."><span class='fa fa-exchange'/></a>  
                        <a href=\"javascript:showConfirm('".base64_encode($pasien->t_pasien_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Hapus Data' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns([ "action"])
            ->make(true);
    }
        public function add(){
        $permission=UsersMenu::getPermission(5);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $groupObat=Obat::where("active",1)->where("active",1)->orderBy("nama")->get();
            $arrObat=array(0=>"");
            foreach ($groupObat AS $ot){
                $arrObat[$ot->m_obat_id]=$ot->nama;
            }
           
              $total=0;
            if(session()->has("detailBayar.detail")){
                $tmpDetail=session()->get("detailBayar.detail");
                foreach ($tmpDetail AS $dt){
                    $dt=(object)$dt;
                    $total+=$dt->total;
                }
            }

         
            return view("adminlte.transaksi.t_pasien._form", compact("permission", "arrObat","total"));
        }
    }


      public function getHarga(Request $request){
        $response=array("status"=>0, "message"=>"", "harga"=>0);
        try{
            if($request->get("obat")==0){
                $response["status"]=1;
                $response["harga"]=0;
            }
            else{
                $obat=Obat::findOrFail($request->get("obat"));
                $response["status"]=1;
                $response["harga"]=number_format($obat->harga,0) ;
            }
        }
        catch (\Exception $e){
            $response["status"]=0;
            $response["message"]=$e->getMessage();
        }
        return json_encode($response);
    }
    public function dataDetail(Request $request){
        //session()->forget("detailBayar.detail");
        //session()->flush();
        if(session()->has("detailBayar.detail")){
            $detail=json_decode(json_encode(session()->get("detailBayar.detail"))) ;
        }
        else{
            $detail=array();
        }
        return DataTables::of($detail)
            ->addColumn("harga", function($detail){
                return number_format($detail->harga,0);
            })
            ->addColumn("total", function($detail){
                return number_format($detail->total, 0);
            })
            ->addColumn("action", function($detail){
                return "<a href=\"javascript:hapusDetail('".$detail->id."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete'><span class='fa fa-trash'/></a> ";
            })
            ->make(true);
    }

    public function tambahDetail(Request $request){
        $response=array("status"=>0, "message"=>"", "harga"=>0);
        try{
            if($request->get("obat")==0){
                throw new \Exception("Pilih Obat");
            }
            $obat=Obat::findOrFail($request->get("obat"));
            $harga=str_replace(",","",$request->get("harga"));
            $qty=str_replace(",","",$request->get("qty"));
            $total=$harga*$qty;
            
            $detail=array(
                "id"=>date("His"),
                "obat"=>$request->get("obat"),
                "nama"=>$obat->nama,
                 "nama"=>$obat->no_hp,
                  "nama"=>$obat->email,
                   "nama"=>$obat->alamat,
                "harga"=>$harga,
                "qty"=>$qty,
                "total"=>$total
            );
            session()->push("detailBayar.detail",$detail);
            $total=0;
            $tmpDetail=session()->get("detailBayar.detail");
            foreach ($tmpDetail AS $dt){
                $dt=(object)$dt;
                $total+=$dt->total;
            }
            $response["total"]=number_format($total,0);
            $response["status"]=1;
        }
        catch (\Exception $e){
            $response["status"]=0;
            $response["message"]=$e->getMessage();
        }
        return json_encode($response);
    }
        
    public function hapusDetail(Request $request){
        $response=array("status"=>0, "message"=>"", "harga"=>0);
        try{
            $tmpDetail=session()->get("detailBayar.detail");
            session()->forget("detailBayar.detail");
            $id=$request->get("id");
            foreach ($tmpDetail AS $td){
                $td=(object)$td;
                if($td->id != $id){
                    $detail=$td;
                    session()->push("detailBayar.detail",$detail);
                }
            }
            $total=0;
            $tmpDetail=session()->get("detailBayar.detail");
            if(sizeof($tmpDetail)>0){
                foreach ($tmpDetail AS $dt){
                    $dt=(object)$dt;
                    $total+=$dt->total;
                }
            }
            $response["total"]=number_format($total,0);
            $response["status"]=1;
        }
        catch (\Exception $e){
            $response["status"]=0;
            $response["message"]=$e->getMessage();
        }
        return json_encode($response);
    }

      public function save(Request $request){
        $response=array("status"=>0, "message"=>"", "harga"=>0);
        try{
           
            $sql="SELECT COUNT(*)+1 AS counter FROM t_pasien WHERE to_char(create_date,'yyyy-mm-dd')::date=CURRENT_DATE";
            $dataCount=DB::connection()->SELECT($sql);
            $count=$dataCount[0]->counter;
            $tgl=date("Y-m-d");
            if($count<10){
                $kode=$tgl."00".$count;
            }
            elseif($count<100){
                $kode=$tgl."0".$count;
            }
            elseif($count<1000){
                $kode=$tgl.$count;
            }
        

            /*Bayar*/
            $bayar=new Pasien();
            $bayar->create_date=date("Y-m-d H:i:s");
            $bayar->nama=$request->get("nama");
            $bayar->email=$request->get("email");
            $bayar->no_hp=$request->get("no_hp");
            $bayar->alamat=$request->get("alamat");
            $hargatotall = $request->get("total");
            $a = str_replace(',', '', $hargatotall);
             $bayar->total_harga=$a;
            $bayar->id_pendaftaran=$kode;
             $bayar->status=1;
            //$bayar->create_date($tanggal);
            $bayar->save();
            $idBayar=$bayar->t_pasien_id;
            /*End Bayar*/

            /*Bayar Detail*/
            
            $detail=json_decode(json_encode(session()->get("detailBayar.detail"))) ;
            //throw new \Exception(json_encode($detail), 1);
            $quantity_total = 0;
            foreach ($detail AS $dt){
                $bayarDetail=new PasienDetail();
                $bayarDetail->t_pasien_id=$idBayar;
                $bayarDetail->m_obat_id=$dt->obat;
                $bayarDetail->harga=$dt->harga;
                $bayarDetail->qty=$dt->qty;
                $bayarDetail->total=$dt->total;
                
                $quantity_total += $dt->qty; 

                $bayarDetail->save();
            }
            /*End Bayar Detail*/

            $bonus = 0;
            if($quantity_total>=30 AND $quantity_total<=99){
                $bonus=2500 * $quantity_total;
            }
            elseif($quantity_total>=100 AND $quantity_total<=150){
                $bonus=4000 * $quantity_total;
            } 
           elseif($quantity_total>150){
                $bonus=6000 * $quantity_total;
            } 

       


            DB::connection()->commit();
            session()->forget("detailBayar.detail");
            $response["status"]=1;
            $response["message"]="Data Telah Disimpan";
            $response["id"]=$idBayar;
        }
        catch (\Exception $e){
            DB::connection()->rollBack();
            $response["status"]=0;
            $response["message"]=$e->getMessage();
        }
        return json_encode($response);
    }

 
    public function delete(Request $request){
        $id=base64_decode($request->get("tmpId"));
        try{
            $kode = Pasien::findOrFail($id);
            $kode->update(['active'=>0]) ;
            return redirect()->route("pasien.list");
        }
        catch (\Exception $e){
            $title="Error";
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            return view("adminlte.error_page",compact("message", "title"));
        }
    }
      public function UbahStatusBayar(Request $request){
        $id=base64_decode($request->get("tmpId2"));
         //var_dump($id);die;
        try{
            $kode = Pasien::findOrFail($id);

            $kode->update(['status'=>2]) ;
            return redirect()->route("pasien.list");
        }
        catch (\Exception $e){
            $title="Error";
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

       public function edit($id){
        $permission=UsersMenu::getPermission(5);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{

            $pasien=Pasien::findOrFail(base64_decode($id));
            $action="Edit";
             $groupObat=Obat::where("active",1)->orderBy("nama")->get();
            $arrObat=array(0=>"");
              $total=0;
            foreach ($groupObat AS $ot){
                $arrObat[$ot->m_obat_id]=$ot->nama;
            }

            
            session()->forget("detailBayar.detail");
            $detailObat=PasienDetail::getDetailObat($pasien->t_pasien_id);
            foreach($detailObat AS $dt){
                    $detail=array(
                    "id"=>$dt->t_pasien_detail_id,
                    "obat"=>$dt->m_obat_id,
                    "nama"=>$dt->nama,
                    "harga"=>$dt->harga,
                    "qty"=>$dt->qty,
                    "total"=>$dt->total
                );
                session()->push("detailBayar.detail",$detail);
                $total+=$dt->total;
            }
        

            return view("adminlte.transaksi.t_pasien._form", compact("permission", "action", "pasien","arrObat","total"));
        }
    }

    public function update(Request $request){

        $this->validate($request,[
            "nama"=>"required",
            "obat"=>"required",
            "qty"=>"required",
            "harga"=>"required",
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
            "obat.required"=>"Product Tidak Boleh Kosong",
             "qty.required"=>"Quantity Tidak Boleh Kosong",
            "harga.required"=>"Harga Tidak Boleh Kosong",
          
        ]);
        $response=array("status"=>0, "message"=>"", "harga"=>0);
        try{
           
            $sql="SELECT COUNT(*)+1 AS counter FROM t_pasien WHERE to_char(create_date,'yyyy-mm-dd')::date=CURRENT_DATE";
            $dataCount=DB::connection()->SELECT($sql);
            $count=$dataCount[0]->counter;
            $tgl=date("Y-m-d");
            if($count<10){
                $kode=$tgl."00".$count;
            }
            elseif($count<100){
                $kode=$tgl."0".$count;
            }
            elseif($count<1000){
                $kode=$tgl.$count;
            }
            
            $obat=Pasien::findOrFail($request->get("t_pasien_id"));
            $arrUpdate=array(
                "obat"=>$request->get("obat"),
                "nama"=>ucwords($request->get("nama")),
                "harga"=>str_replace(",", "", $request->get("harga")),
                "qty"=>str_replace(",","", $request->get("qty")),
                "total"=>str_replace(",","", $request->get("total"))
            );
            $obat->update($arrUpdate);


                      
            $idpasienn=$request->get("t_pasien_id");
            $sqlHapus="DELETE FROM t_pasien WHERE t_pasien_id=".$idpasienn;
            $conn->statement($sqlHapus);

            $detail=json_decode(json_encode(session()->get("detailBayar.detail"))) ;
            //throw new \Exception(json_encode($detail), 1);
             $quantity_total = 0;
           foreach ($detail AS $dt){
                $bayarDetail=new PasienDetail();
                $bayarDetail->t_pasien_id=$idBayar;
                $bayarDetail->m_obat_id=$dt->obat;
                $bayarDetail->harga=$dt->harga;
                $bayarDetail->qty=$dt->qty;
                $bayarDetail->total=$dt->total;
                
                $quantity_total += $dt->qty; 

                $bayarDetail->save();
            }


            /*End Bayar Detail*/

            DB::connection()->commit();
            session()->forget("detailBayar.detail");
            $response["status"]=1;
            $response["message"]="Data Telah Disimpan";
            $response["id"]=$idBayar;
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

 
}