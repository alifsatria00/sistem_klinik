<?php

namespace App\Http\Controllers;

use App\Wilayah;
use App\UsersMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class WilayahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_read==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            return view("adminlte.master.m_wilayah.index", compact("permission"));
        }

    }

    public function data(){
        $wilayah=Wilayah::get_list_wilayah();
        $permission=UsersMenu::getPermission(2);
        return DataTables::of($wilayah)

            ->addColumn("action", function ($wilayah){
                $permission=UsersMenu::getPermission(2);
                $editable="";
                $deletable="";
                //style='visibility: hidden'
                if($permission[0]->_update==0){
                    $editable="disabled='disabled' onclick='return false;'";
                }
                if($permission[0]->_delete==0){
                    $deletable="disabled='disabled' onclick='return false;'";
                }
                return "<a href='".route("wilayah.edit", base64_encode($wilayah->m_wilayah_id))."' class='btn btn-sm btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit' ".$editable."><span class='fa fa-edit'/></a> <a href=\"javascript:showConfirm('".base64_encode($wilayah->m_wilayah_id)."')\"  class='btn btn-sm btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete' ".$deletable."><span class='fa fa-trash'/></a> ";
            })
            ->rawColumns(["action"])
            ->make(true);
    }

    public function add(){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_add==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
           
            return view("adminlte.master.m_wilayah._form", compact("permission"));
        }
    }

    public function save(Request $request){
        $this->validate($request,[
            "nama"=>"required",

        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",

        ]);

        try{

            $wilayah=new Wilayah();
            $wilayah->nama=ucwords($request->get("nama"));
            $wilayah->save();
            return redirect()->route("wilayah.list");
        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function edit($id){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_update==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            $wilayah=Wilayah::findOrFail(base64_decode($id));
            $action="Edit";
            
            return view("adminlte.master.m_wilayah._form", compact("permission", "action", "wilayah"));
        }
    }

    public function update(Request $request){
        $this->validate($request,[
           "nama"=>"required",
          
        ],[
            "nama.required"=>"Nama Tidak Boleh Kosong",
   
        ]);
        try{

              $wilayah=Wilayah::findOrFail($request->get("m_wilayah_id"));
            $arrUpdate=array(
                "nama"=>ucwords($request->get("nama")),
    
            );
            $wilayah->update($arrUpdate);
            return redirect()->route("wilayah.list");

        }
        catch (\Exception $e){
            $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
            $title="Error";
            return view("adminlte.error_page",compact("message", "title"));
        }
    }

    public function delete(Request $request){
        $permission=UsersMenu::getPermission(2);
        if($permission[0]->_delete==0){
            $title="Akses";
            $message="Anda Tidak Mempunyai Akses Untuk Membuka Halaman Ini";
            return view("adminlte.error_page", compact("title", "message"));
        }
        else{
            try{
                $wilayah=Wilayah::findOrFail(base64_decode($request->get("tmpId")));
                $wilayah->update(array("active"=>0));
                return redirect()->route("wilayah.list");
            }
            catch (\Exception $e){
                $message=$e->getMessage()."<br>".$e->getFile()."<br>".$e->getLine();
                $title="Error";
                return view("adminlte.error_page",compact("message", "title"));
            }
        }
    }
}