<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
class Obat extends Model
{
    protected $table="m_obat";
    protected $primaryKey="m_obat_id";
    protected $fillable=["nama","active", "create_date", "update_date","harga"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

public static function get_list_obat(){
        $sql="SELECT *
          FROM m_obat
                WHERE m_obat.active=1";
        $data=DB::connection()->select($sql);
        return $data;
    }
    
}
