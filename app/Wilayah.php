<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
class Wilayah extends Model
{
    protected $table="m_wilayah";
    protected $primaryKey="m_wilayah_id";
    protected $fillable=["nama","active", "create_date", "update_date"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

public static function get_list_wilayah(){
        $sql="SELECT *
          FROM m_wilayah
                WHERE m_wilayah.active=1";
        $data=DB::connection()->select($sql);
        return $data;
    }
    
}
