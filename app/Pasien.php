<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Pasien extends Model
{
    protected $table="t_pasien";
    protected $primaryKey="t_pasien_id";
    protected $fillable=["nama", "alamat", "active", "create_date", "update_date", "no_hp", "email","total_harga","status","id_pendaftaran"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    public static function get_list_t_pasien(){
        $sql="SELECT t_pasien.t_pasien_id,
			t_pasien.id_pendaftaran,
			t_pasien.nama,
            t_pasien.alamat,
            t_pasien.no_hp,
            t_pasien.email,
			t_pasien.status,
            t_pasien.create_date,
			CASE WHEN t_pasien.status=1 THEN 'BELUM LUNAS'::VARCHAR
			ELSE CASE WHEN t_pasien.status=2 THEN 'LUNAS'::VARCHAR
             END END AS status_text,
			(SELECT SUM(t_pasien_detail.total) FROM t_pasien_detail WHERE t_pasien_detail.t_pasien_id=t_pasien.t_pasien_id) AS total_harga
			FROM t_pasien
			WHERE t_pasien.active=1 
			ORDER BY t_pasien.create_date DESC";
        $data=DB::connection()->select($sql);
        return $data;
    }
        public static function get_pasien_with_nama($t_pasien_id){
        $sql="SELECT t_pasien.t_pasien_id,
            t_pasien.create_date,
            t_pasien.nama,
            t_pasien.no_hp,
            t_pasien.alamat,
            t_pasien.email,
            t_pasien.total_harga,
            t_pasien.status,
       CASE WHEN t_pasien.status=1 THEN 'BELUM LUNAS'::VARCHAR
            ELSE CASE WHEN t_pasien.status=2 THEN 'LUNAS'::VARCHAR
             END END AS status_text,
            t_pasien.email,
            t_pasien.no_hp
            FROM t_pasien
            WHERE t_pasien.t_pasien_id=".$t_pasien_id;
        $data=DB::connection()->select($sql);
        return $data[0];
    }
}
