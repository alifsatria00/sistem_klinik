<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class PasienDetail extends Model
{
    protected $table="t_pasien_detail";
    protected $primaryKey="t_pasien_detail_id";
    protected $fillable=["t_pasien_id", "m_obat_id", "qty","harga","total","active", "create_date", "update_date"];
    const CREATED_AT = "create_date";
    const UPDATED_AT = "update_date";

    
   public static function getDetailObat($t_pasien_id){
   		$sql="SELECT t_pasien_detail.t_pasien_detail_id,
				t_pasien_detail.m_obat_id,
				m_obat.nama,
				t_pasien_detail.harga,
				t_pasien_detail.qty,
				t_pasien_detail.total
				FROM t_pasien_detail 
				INNER JOIN m_obat on m_obat.m_obat_id=t_pasien_detail.m_obat_id
				WHERE t_pasien_id=".$t_pasien_id;

		$data=DB::connection()->select($sql);
		return $data;
   } 
     public static function get_pasiendetail($idPendaftaran){

        $sql="SELECT * FROM t_pasien_detail WHERE t_pasien_id=".$idPendaftaran;

        $data=DB::connection()->select($sql);
        return $data;
    }

}
