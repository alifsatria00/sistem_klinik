@extends('adminlte.layout.app')

@section("style")
@endsection

@section("content")
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Jenis Tindakan
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route("home") !!}"><i class="fa fa-dashboard"></i> Master</a></li>
            <li class="active"><a href="{!! route("jenistindakan.list") !!}">   Jenis Tindakan</a></li>
            <li class="active"> Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-title"></div>
                        <div class="pull-right box-tools">
                            <a href="{!! route("jenistindakan.list") !!}" class="btn btn-danger btn-sm" data-toggle="tooltip"
                               title="Cancel">
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:$('#form-user').submit()" class="btn btn-success btn-sm"
                               data-toggle="tooltip" title="{!! isset($action)?$action:"Save" !!}">
                                <i class="fa {!! isset($action)?"fa-edit":"fa-save" !!}"></i>
                            </a>
                        </div>
                    </div>

                    <div class="box-body pad">
                        {!! Form::open(['url' => isset($action)?route("jenistindakan.update"):route("jenistindakan.save"), 'method' => 'post', 'id'=>'form-user', "enctype"=>"multipart/form-data"]) !!}
                        {!! Form::hidden("m_jenis_tindakan_id", isset($jenistindakan)?$jenistindakan->m_jenis_tindakan_id:null) !!}
                        <div class="row">
                        
                              <div class="col-md-12 col-12">
                                <div class="form-group {!! $errors->has("nama")?"has-error":"" !!}">
                                    {!! Form::label("nama", "Jenis Tindakan*") !!}
                                    {!! Form::text("nama", isset($jenistindakan)?$jenistindakan->nama:null, ["class"=>"form-control", "style"=>"text-transform: capitalize", "required"]) !!}
                                    {!! $errors->first("nama", "<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                           
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection

@section("script")
    <script>
        $(function () {

        });
    </script>
@endsection