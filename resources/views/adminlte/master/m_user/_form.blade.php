@extends('adminlte.layout.app')

@section("style")
@endsection

@section("content")
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route("home") !!}"><i class="fa fa-dashboard"></i> Master</a></li>
            <li>User Management</li>
            <li class="active"><a href="{!! route("user.list") !!}"> User</a></li>
            <li class="active"> Add</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-title"></div>
                        <div class="pull-right box-tools">
                            <a href="{!! route("user.list") !!}" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Cancel">
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:$('#form-user').submit()" class="btn btn-success btn-sm" data-toggle="tooltip" title="{!! isset($action)?$action:"Save" !!}">
                                <i class="fa {!! isset($action)?"fa-edit":"fa-save" !!}"></i>
                            </a>
                        </div>
                    </div>

                    <div class="box-body pad">
                        {!! Form::open(['url' => isset($action)?route("user.update"):route("user.save"), 'method' => 'post', 'id'=>'form-user', "enctype"=>"multipart/form-data"]) !!}
                        {!! Form::hidden("id_user",isset($user)?$user->id:null) !!}
                        <div class="row">
                     
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="form-group {!! $errors->has("email")?"has-error":"" !!}">
                                    {!! Form::label("email","Email *") !!}
                                    {!! Form::text("email",isset($user)?$user->email:null,["class"=>"form-control", "required"]) !!}
                                    {!! $errors->first("email","<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="form-group {!! $errors->has("name")?"has-error":"" !!}">
                                    {!! Form::label("name", "Nama *") !!}
                                    {!! Form::text("name", isset($user)?$user->name:null,["class"=>"form-control", "style"=>"text-transform: capitalize", "required"]) !!}
                                    {!! $errors->first("name","<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                        </div>
               
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="form-group {!! $errors->has("password")?"has-error":"" !!}">
                                    {!! Form::label("password", "Password *") !!}
                                    {!! Form::password("password",["class"=>"form-control", "required"]) !!}
                                    {!! $errors->first("password", "<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-12">
                                <div class="form-group {!! $errors->has("password_confirmation")?"has-error":"" !!}">
                                    {!! Form::label("password_confirmation", "Konfirmasi Password *") !!}
                                    {!! Form::password("password_confirmation",["class"=>"form-control", "required"]) !!}
                                    {!! $errors->first("password_confirmation", "<p class='help-block'>:message</p>") !!}
                                </div>
                            </div>
                        </div>
  
                        <div class="box">
                            <div class="box-header"></div>
                            <table id="menu" class="table table-bordered table-striped table-responsive">
                                <thead>
                                <tr>
                                    <th>Group</th>
                                    <th>Sub</th>
                                    <th>Menu</th>
                                    <th>Read</th>
                                    <th>Add</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection

@section("script")
    <script>
        $(function () {
            $('#menu').DataTable({
                serverSide: true,
                processing: true,
                ajax: "{{route('user.menu',[isset($user)?$user->id:0])}}",
                columns: [
                    {data:'_group'},
                    {data:'_sub'},
                    {data:'nama'},
                    {data:'_read', orderable:false, searchable:false},
                    {data:'_add', orderable:false, searchable:false},
                    {data:'_update', orderable:false, searchable:false},
                    {data:'_delete', orderable:false, searchable:false}
                ],
                order:[[0,'asc'],[1,'asc'],[2,'asc']],
                pageLength:100,
                lengthMenu:[100,500,1000],
                responsive: true,
                autoWidth:false,
                deferRender: true,
                fixedColumns: true
            });
        });
    </script>
@endsection