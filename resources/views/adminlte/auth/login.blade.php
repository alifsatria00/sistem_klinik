@extends('adminlte.layout.login')

@section("content")
    <!-- Content Header (Page header) -->
    <div class="login-box">
        <div class="login-logo">
            <a href="{!! route("home") !!}"><b>KLINIK</b> ALIF SATRIA</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            {!! Form::open(['url' => url('login'), 'method' => 'post', 'id'=>'form-galery', "enctype"=>"multipart/form-data"]) !!}
            <div class="form-group has-feedback">
                {!! Form::email("email","",["class"=>"form-control", "placeholder"=>"Email"]) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                {!! Form::password("password",["class"=>"form-control", "placeholder"=>"Password"]) !!}
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck check">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}

            <a href="#">I forgot my password</a><br>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- /.content -->
@endsection
