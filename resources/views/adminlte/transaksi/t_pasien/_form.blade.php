@extends('adminlte.layout.app')

@section("style")
    <style>
        .box-contact{
            border-bottom: aliceblue 0.5px solid;
            cursor: pointer;
        }
        .box-contact:hover{
            /*background-color: #f8f8ff;*/
        }

        .modal-loading {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 )
            url('{!! asset("logo/ajax-loader.gif") !!}')
            50% 50%
            no-repeat;
        }

        /* When the body has the loading class, we turn
           the scrollbar off with overflow:hidden */
        body.loading .modal-loading {
            overflow: hidden;
        }

        /* Anytime the body has the loading class, our
           modal element will be visible */
        body.loading .modal-loading {
            display: block;
        }
    </style>
@endsection

@section("content")
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pendaftaran Pasien
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route("home") !!}"><i class="fa fa-dashboard"></i> Transaksi</a></li>
            <li class="active"><a href="{!! route("pasien.list") !!}">   Pendaftaran Pasien</a></li>
            <li class="active">{!! isset($action)?" Add":" Edit" !!}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="box box-info">
                    <div class="box-header">
                        <div class="box-title"></div>
                        <div class="pull-right box-tools">
                            <a href="{!! route("pasien.list") !!}" class="btn btn-danger btn-sm" data-toggle="tooltip"
                               title="Cancel">
                                <i class="fa fa-close"></i>
                            </a>
                            <a href="javascript:simpanPembayaran()" class="btn btn-success btn-sm"
                               data-toggle="tooltip" title="{!! isset($action)?$action:"Save" !!}">
                                <i class="fa {!! isset($action)?"fa-edit":"fa-save" !!}"></i>
                            </a>
                        </div>
                    </div>

                    <div class="box-body pad" id="form-payment">

                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("nama")?"has-error":"" !!}">
                                        {!! Form::label("nama", "Nama *") !!}
                                        {!! Form::text("nama", " ", ["class"=>"form-control"]) !!}
                                        {!! $errors->first("nama", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                   <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("alamat")?"has-error":"" !!}">
                                        {!! Form::label("alamat", "Alamat *") !!}
                                        {!! Form::text("alamat", " ", ["class"=>"form-control"]) !!}
                                        {!! $errors->first("alamat", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                    <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("no_hp")?"has-error":"" !!}">
                                        {!! Form::label("no_hp", "No HP *") !!}
                                        {!! Form::text("no_hp", " ", ["class"=>"form-control"]) !!}
                                        {!! $errors->first("no_hp", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                    <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("email")?"has-error":"" !!}">
                                        {!! Form::label("email", "Email *") !!}
                                        {!! Form::text("email", " ", ["class"=>"form-control"]) !!}
                                        {!! $errors->first("email", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                    <div class="col-md-6 col-sm-12">
                                      <div class="form-group {!! $errors->has("obat")?"has-error":"" !!}">
                                        {!! Form::label("obat", "Obat *") !!}
                                        {!! Form::select("obat", $arrObat, null, ["class"=>"form-control select"]) !!}
                                        {!! $errors->first("obat", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                              
                            </div>
                         
                            <div class="row">
                                 <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("qty")?"has-error":"" !!}">
                                        {!! Form::label("qty", "Quantity *") !!}
                                        {!! Form::text("qty", 1, ["class"=>"form-control numeric"]) !!}
                                        {!! $errors->first("qty", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group {!! $errors->has("harga")?"has-error":"" !!}">
                                        {!! Form::label("harga", "Harga") !!}
                                        {!! Form::text("harga", 0, ["class"=>"form-control", "readonly"]) !!}
                                        {!! $errors->first("harga", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                                 
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    {!! Form::button("Tambah",["class"=>"btn btn-md btn-success", "id"=>"btnTambah"]) !!}
                                </div>
                            </div>
                                <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group {!! $errors->has("total")?"has-error":"" !!}">
                                        {!! Form::label("total", "Total") !!}
                                        {!! Form::text("total", $total, ["class"=>"form-control numeric", "readonly", "style"=>"font-weight: bolder"]) !!}
                                        {!! $errors->first("total", "<p class='help-block'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div>
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>Nama Obat</th>
                                                <th>Harga</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                                <th>Hapus</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->

    <div class="modal-loading"><!-- Place at bottom of page --></div>

    <!--Modal Success-->
    <div id="myModalSuccess" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Success</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p id="pesanSukses">Do You Want To Delete This Data?.</p>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </div>
    <!--End Modal Success-->

    <!--Modal Error-->
    <div id="myModalError" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="box box-danger box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Error</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p id="pesanError">Do You Want To Delete This Data?.</p>
                    </div>
                    <div class="box-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </div>
    <!--End Modal Error-->
@endsection

@section("script")
    <script src="{!! asset("adminlte/bower_components/autonumeric/autoNumeric-1.9.26.js") !!}"></script>
    <script>
        $(function () {
            $('.numeric').autoNumeric({
                aSep:',',
                aDec:'.',
                aSign:'',
                pSign:'p', //p->left, s->right
                vMin:'0',
                vMax: '999999999999999',
                mDec:'0',
                aPad:true
            }).css('text-align', 'right')
                .bind('blur',function(){
                    var val= $.trim($(this).val());
                    if(val==''){
                        $(this).val('0');
                    }
                });

                $("#obat").on("change", function(){
                    $body = $("body");
                    var obat=$(this).val();
                    //alert(product);
                    $.ajax({
                        url:"{!! route("pasien.getHarga") !!}",
                        data:$.param({"_token": "{{ csrf_token() }}", "obat":obat}),
                        type:"post",
                        dataType:"json",
                        beforeSend:function(){$body.addClass("loading");},
                        success:function(data, textStatus){
                            var status=data.status;
                            if(status==1){
                                $("#harga").val(data.harga);
                            }
                            else{
                                $("#pesanError").text(data.message);
                                $("#myModalError").modal("show");
                            }
                            $body.removeClass("loading");
                        },
                        error:function(xmlEr, typeEr, except){
                            $body.removeClass("loading");
                            $("#pesanError").text(xmlEr.responseText);
                            $("#myModalError").modal("show");
                        }
                    });
                });

            var tableSchedule=$('#example1').DataTable({
                serverSide: true,
                processing: true,
                ajax: "{{route('pasien.dataDetail')}}",
                columns: [
                    {data: 'nama'},
                    {data: 'harga'},
                    {data: 'qty'},
                    {data: 'total'},
                    {data: 'action', orderable: false, searchable: false}
                ],
                pasien: [[0, 'asc']],
                pageLength: 10,
                lengthMenu: [10, 50, 100],
            });

            $("#btnTambah").on("click", function () {
                $body = $("body");
                var obat=$("#obat").val();
                var harga=$("#harga").val();
                var qty=$("#qty").val();
                $.ajax({
                    url:"{!! route("pasien.tambahDetail") !!}",
                    data:$.param({"_token": "{{ csrf_token() }}", "obat":obat, "harga":harga, "qty":qty}),
                    type:"post",
                    dataType:"json",
                    beforeSend:function(){$body.addClass("loading");},
                    success:function(data, textStatus){
                        var status=data.status;
                        if(status==1){
                            $("#obat").select2('val','0');
                            $("#harga").val(0);
                            $("#qty").val(1);
                            $("#total").val(data.total);
                            //$("#harga").val(data.harga);
                            $('#example1').DataTable().ajax.reload();
                        }
                        else{
                            $("#pesanError").text(data.message);
                            $("#myModalError").modal("show");
                        }
                        $body.removeClass("loading");
                    },
                    error:function(xmlEr, typeEr, except){
                        $body.removeClass("loading");
                        $("#pesanError").text(xmlEr.responseText);
                        $("#myModalError").modal("show");
                    }
                });
            });

        });

        function hapusDetail(id){
            $body = $("body");
            $.ajax({
                url:"{!! route("pasien.hapusDetail") !!}",
                data:$.param({"_token": "{{ csrf_token() }}", "id":id}),
                type:"post",
                dataType:"json",
                beforeSend:function(){$body.addClass("loading");},
                success:function(data, textStatus){
                    var status=data.status;
                    if(status==1){
                        //$("#harga").val(data.harga);
                        $("#total").val(data.total);
                        $('#example1').DataTable().ajax.reload();
                    }
                    else{
                        $("#pesanError").text(data.message);
                        $("#myModalError").modal("show");
                    }
                    $body.removeClass("loading");
                },
                error:function(xmlEr, typeEr, except){
                    $body.removeClass("loading");
                    $("#pesanError").text(xmlEr.responseText);
                    $("#myModalError").modal("show");
                }
            });
        }

        function simpanPembayaran() {
            $body = $("body");
            var total=$("#total").val();
            var nama=$("#nama").val();
            var email=$("#email").val();
            var no_hp=$("#no_hp").val();
            var alamat=$("#alamat").val();
            $.ajax({
                url:"{!! route("pasien.save") !!}",
                data:$.param({"_token": "{{ csrf_token() }}", "total":total, "nama":nama, "email":email, "no_hp":no_hp, "alamat":alamat}),
                type:"post",
                dataType:"json",
                beforeSend:function(){$body.addClass("loading");},
                success:function(data, textStatus){
                    $body.removeClass("loading");
                    var status=data.status;
                    if(status==1){
                        //$("#harga").val(data.harga);
                        $("#total").val(0);
                        //cetakFaktur(data.id);
                        $('#example1').DataTable().ajax.reload();
                        $("#pesanSukses").text(data.message);
                        $("#myModalSuccess").modal("show");
                    }
                    else{
                        $("#pesanError").text(data.message);
                        $("#myModalError").modal("show");
                    }
                },
                error:function(xmlEr, typeEr, except){
                    $body.removeClass("loading");
                    $("#pesanError").text(xmlEr.responseText);
                    $("#myModalError").modal("show");
                }
            });
        }

 
    </script>
@endsection