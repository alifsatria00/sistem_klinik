@extends('adminlte.layout.login')
@section("content")
    <div class="box">
        <div class="box-header">
            <div class="box-title">
                <h3><i class="fa fa-warning text-red"></i> {!! $title !!}.</h3>
            </div>
            <div class="box-body">
                <p>
                    {!! $message !!}.
                </p>
            </div>
            <div class="box-footer">
                <button class="btn btn-lg btn-info" onclick="javascript:window.history.back();"><span class="fa fa-backward"></span> Back</button>
            </div>
        </div>
    </div>
<div class="error-page align-center">

</div>
@endsection