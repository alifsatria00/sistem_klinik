<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
@include("adminlte.layout.head")
<!-- Styles -->
    @yield('style')
</head>
<body class="hold-transition skin-blue layout-top-nav">

<header class="main-header">
    <header class="main-header">
        <nav class="navbar navbar-static-top" style="background-color: #f5f8fa">
            <div class="container">
                <div class="navbar-header">
                    <a href="{!! route("booking.home") !!}" class="navbar-brand">
                        <img src="{!! asset("img/logo_sas.png") !!}" style="height: 30px; margin-top: -5px">
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
</header>

{{--@include("adminlte.layout.sidebar")--}}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="height: 100%">
    @yield("content")
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    @include("adminlte.layout.footer")
</footer>

@include("adminlte.layout.script")
@yield("script")
</body>
</html>