<!-- jQuery 3 -->
<script src="{!! asset("adminlte/bower_components/jquery/dist/jquery.min.js") !!}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{!! asset("adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js") !!}"></script>
<!-- FastClick -->
<script src="{!! asset("adminlte/bower_components/fastclick/lib/fastclick.js") !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset("adminlte/dist/js/adminlte.min.js") !!}"></script>
<!-- Sparkline -->
<script src="{!! asset("adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") !!}"></script>
<!-- jvectormap  -->
<script src="{!! asset("adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") !!}"></script>
<script src="{!! asset("adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js") !!}"></script>
<!-- SlimScroll -->
<script src="{!! asset("adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js") !!}"></script>
<!-- ChartJS -->
<script src="{!! asset("adminlte/bower_components/chart.js/Chart.js") !!}"></script>

<!-- FLOT CHARTS -->
<script src="{!! asset("adminlte/bower_components/Flot/jquery.flot.js") !!}"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="{!! asset("adminlte/bower_components/Flot/jquery.flot.resize.js") !!}"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="{!! asset("adminlte/bower_components/Flot/jquery.flot.pie.js") !!}"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="{!! asset("adminlte/bower_components/Flot/jquery.flot.categories.js") !!}"></script>
<!-- Page script -->

<!--select2-->
<script src="{!! asset("adminlte/plugins/select2/select2.js") !!}"></script>
<!--select2-->

<!-- iCheck -->
<script src="{!! asset("adminlte/plugins/iCheck/icheck.min.js") !!}"></script>

<!-- bootstrap datepicker -->
<script src="{!! asset("adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") !!}"></script>

<!-- DataTables -->
<script src="{!! asset("adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js") !!}"></script>
<script src="{!! asset("adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") !!}"></script>
<!-- SlimScroll -->
<script src="{!! asset("adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js") !!}"></script>
<!-- FastClick 
<script src="{!! asset("adminlte/bower_components/fastclick/lib/fastclick.js") !!}"></script>
-->
<!-- The core Firebase JS SDK is always required and must be listed first 
<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-app.js"></script>
-->
<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app 

<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyDXGL7GUt2yVY3S3lLNlCQv0k_El1FQEr0",
        authDomain: "sas-prefered-guest.firebaseapp.com",
        databaseURL: "https://sas-prefered-guest.firebaseio.com",
        projectId: "sas-prefered-guest",
        storageBucket: "sas-prefered-guest.appspot.com",
        messagingSenderId: "64444378318",
        appId: "1:64444378318:web:a662b3de22bc525c"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>
-->
<script>
    //var $j=jQuery.noConflict(true);

    $('[data-toggle="tooltip"]').tooltip();

    $( ".select" ).select2({
        theme: "bootstrap"
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format:"dd-mm-yyyy"
    });

    $('.check').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
    });
</script>

