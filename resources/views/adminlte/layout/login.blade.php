<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
@include("adminlte.layout.head")
<!-- Styles -->
    @yield('style')
</head>
<body class="hold-transition login-page">

<!-- Content Wrapper. Contains page content -->
@yield("content")
<!-- /.content-wrapper -->

@include("adminlte.layout.script")
@yield("script")
</body>
</html>