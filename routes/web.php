<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication Routes...
$this->get('/', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->get('logout', 'Auth\LoginController@logout')->name('auth.logout');
Auth::routes();
// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');
Route::get('/', function () {
    return view('adminlte.auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(["prefix"=>"master"], function (){
    Route::group(["prefix"=>"user"], function (){
        Route::get("list", "UserController@index")->name("user.list");
        Route::get("data", "UserController@data")->name("user.data");
        Route::get("add", "UserController@add")->name("user.add");
        Route::get("menu/{id}", "UserController@menu")->name("user.menu");
        Route::post("save", "UserController@save")->name("user.save");
        Route::get("edit/{id}", "UserController@edit")->name("user.edit");
        Route::post("update", "UserController@update")->name("user.update");
        Route::post("delete", "UserController@delete")->name("user.delete");
    });
        Route::group(["prefix"=>"wilayah"], function (){
        Route::get("list", "WilayahController@index")->name("wilayah.list");
        Route::get("data", "WilayahController@data")->name("wilayah.data");
        Route::get("add", "WilayahController@add")->name("wilayah.add");
        Route::get("menu/{id}", "WilayahController@menu")->name("wilayah.menu");
        Route::post("save", "WilayahController@save")->name("wilayah.save");
        Route::get("edit/{id}", "WilayahController@edit")->name("wilayah.edit");
        Route::post("update", "WilayahController@update")->name("wilayah.update");
        Route::post("delete", "WilayahController@delete")->name("wilayah.delete");
    });
             Route::group(["prefix"=>"jenistindakan"], function (){
        Route::get("list", "JenisTindakanController@index")->name("jenistindakan.list");
        Route::get("data", "JenisTindakanController@data")->name("jenistindakan.data");
        Route::get("add", "JenisTindakanController@add")->name("jenistindakan.add");
        Route::get("menu/{id}", "JenisTindakanController@menu")->name("jenistindakan.menu");
        Route::post("save", "JenisTindakanController@save")->name("jenistindakan.save");
        Route::get("edit/{id}", "JenisTindakanController@edit")->name("jenistindakan.edit");
        Route::post("update", "JenisTindakanController@update")->name("jenistindakan.update");
        Route::post("delete", "JenisTindakanController@delete")->name("jenistindakan.delete");
    });
                    Route::group(["prefix"=>"obat"], function (){
        Route::get("list", "ObatController@index")->name("obat.list");
        Route::get("data", "ObatController@data")->name("obat.data");
        Route::get("add", "ObatController@add")->name("obat.add");
        Route::get("menu/{id}", "ObatController@menu")->name("obat.menu");
        Route::post("save", "ObatController@save")->name("obat.save");
        Route::get("edit/{id}", "ObatController@edit")->name("obat.edit");
        Route::post("update", "ObatController@update")->name("obat.update");
        Route::post("delete", "ObatController@delete")->name("obat.delete");
    });
     });
Route::group(["prefix"=>"transaksi"], function(){

    Route::group(["prefix"=>"pasien"], function (){
        Route::get("list", "PasienController@index")->name("pasien.list");
        Route::get("data", "PasienController@data")->name("pasien.data");
        Route::get("add", "PasienController@add")->name("pasien.add");
        Route::get("menu/{id}", "PasienController@menu")->name("pasien.menu");
        Route::post("save", "PasienController@save")->name("pasien.save");
        Route::get("edit/{id}", "PasienController@edit")->name("pasien.edit");
        Route::post("update", "PasienController@update")->name("pasien.update");
        Route::post("delete", "PasienController@delete")->name("pasien.delete");
         Route::post("UbahStatusBayar", "PasienController@UbahStatusBayar")->name("pasien.UbahStatusBayar");
        Route::post("getHarga", "PasienController@getHarga")->name("pasien.getHarga");
        Route::get("dataDetail", "PasienController@dataDetail")->name("pasien.dataDetail");
        Route::post("tambahDetail", "PasienController@tambahDetail")->name("pasien.tambahDetail");
        Route::post("hapusDetail", "PasienController@hapusDetail")->name("pasien.hapusDetail");

    });
     });
