PGDMP         2                y            klinik2    12.7    12.7 =    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16571    klinik2    DATABASE     �   CREATE DATABASE klinik2 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE klinik2;
                postgres    false            �            1255    16719    get_new_m_jenis_tindakan_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_jenis_tindakan_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_jenis_tindakan_id)+1,1) FROM m_jenis_tindakan INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 4   DROP FUNCTION public.get_new_m_jenis_tindakan_id();
       public          postgres    false            �            1255    16680    get_new_m_menu_detail_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_menu_detail_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_menu_detail_id)+1,1) FROM m_menu_detail INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 1   DROP FUNCTION public.get_new_m_menu_detail_id();
       public          postgres    false            �            1255    16682    get_new_m_menu_group_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_menu_group_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_menu_group_id)+1,1) FROM m_menu_group INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 0   DROP FUNCTION public.get_new_m_menu_group_id();
       public          postgres    false            �            1255    16684    get_new_m_menu_sub_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_menu_sub_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_menu_sub_id)+1,1) FROM m_menu_sub INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 .   DROP FUNCTION public.get_new_m_menu_sub_id();
       public          postgres    false            �            1255    16729    get_new_m_obat_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_obat_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_obat_id)+1,1) FROM m_obat INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 *   DROP FUNCTION public.get_new_m_obat_id();
       public          postgres    false            �            1255    16710    get_new_m_tipe_tindakan_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_tipe_tindakan_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_tipe_tindakan_id)+1,1) FROM m_tipe_tindakan INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 3   DROP FUNCTION public.get_new_m_tipe_tindakan_id();
       public          postgres    false            �            1255    16697    get_new_m_wilayah_id()    FUNCTION     �   CREATE FUNCTION public.get_new_m_wilayah_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(m_wilayah_id)+1,1) FROM m_wilayah INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 -   DROP FUNCTION public.get_new_m_wilayah_id();
       public          postgres    false            �            1255    16762    get_new_t_pasien_detail_id()    FUNCTION     �   CREATE FUNCTION public.get_new_t_pasien_detail_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(t_pasien_detail_id)+1,1) FROM t_pasien_detail INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 3   DROP FUNCTION public.get_new_t_pasien_detail_id();
       public          postgres    false            �            1255    16743    get_new_t_pasien_id()    FUNCTION     �   CREATE FUNCTION public.get_new_t_pasien_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(t_pasien_id)+1,1) FROM t_pasien INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 ,   DROP FUNCTION public.get_new_t_pasien_id();
       public          postgres    false            �            1255    16678    get_new_users_menu_id()    FUNCTION     �   CREATE FUNCTION public.get_new_users_menu_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(users_menu_id)+1,1) FROM users_menu INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 .   DROP FUNCTION public.get_new_users_menu_id();
       public          postgres    false            �            1255    16742    gget_new_t_pasien_id()    FUNCTION     �   CREATE FUNCTION public.gget_new_t_pasien_id() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_id NUMERIC;
BEGIN
	SELECT COALESCE(MAX(t_pasien_id)+1,1) FROM t_pasien INTO tmp_id;
	RETURN tmp_id;
END;
$$;
 -   DROP FUNCTION public.gget_new_t_pasien_id();
       public          postgres    false            �            1255    16675    trigger_set_timestamp()    FUNCTION     �   CREATE FUNCTION public.trigger_set_timestamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.created_at = NOW();
  RETURN NEW;
END;
$$;
 .   DROP FUNCTION public.trigger_set_timestamp();
       public          postgres    false            �            1259    16699    m_jenis_tindakan    TABLE     H  CREATE TABLE public.m_jenis_tindakan (
    m_jenis_tindakan_id numeric(20,0) DEFAULT public.get_new_m_jenis_tindakan_id() NOT NULL,
    active numeric(1,0) DEFAULT 1,
    create_date timestamp without time zone DEFAULT now(),
    update_date timestamp without time zone DEFAULT now(),
    nama character varying(50) NOT NULL
);
 $   DROP TABLE public.m_jenis_tindakan;
       public         heap    postgres    false    225            �            1259    16650    m_menu_detail    TABLE     W  CREATE TABLE public.m_menu_detail (
    m_menu_detail_id numeric(20,0) DEFAULT public.get_new_m_menu_detail_id() NOT NULL,
    m_menu_group_id numeric(20,0) NOT NULL,
    m_menu_sub_id numeric(20,0) NOT NULL,
    nama character varying(50) NOT NULL,
    route_name character varying(50) NOT NULL,
    active numeric(1,0) DEFAULT 1 NOT NULL
);
 !   DROP TABLE public.m_menu_detail;
       public         heap    postgres    false    220            �            1259    16655    m_menu_group    TABLE     �   CREATE TABLE public.m_menu_group (
    m_menu_group_id numeric(20,0) DEFAULT public.get_new_m_menu_group_id() NOT NULL,
    nama character varying(50) NOT NULL,
    active numeric(1,0) DEFAULT 1 NOT NULL,
    fa character varying(50) NOT NULL
);
     DROP TABLE public.m_menu_group;
       public         heap    postgres    false    221            �            1259    16660 
   m_menu_sub    TABLE       CREATE TABLE public.m_menu_sub (
    m_menu_sub_id numeric(20,0) DEFAULT public.get_new_m_menu_sub_id() NOT NULL,
    m_menu_group_id numeric(20,0) NOT NULL,
    nama character varying(50) NOT NULL,
    active numeric(1,0) NOT NULL,
    fa character varying(50) NOT NULL
);
    DROP TABLE public.m_menu_sub;
       public         heap    postgres    false    222            �            1259    16721    m_obat    TABLE     D  CREATE TABLE public.m_obat (
    m_obat_id numeric(20,0) DEFAULT public.get_new_m_obat_id() NOT NULL,
    nama character varying(50) NOT NULL,
    active numeric(1,0) DEFAULT 1,
    create_date timestamp without time zone DEFAULT now(),
    update_date timestamp without time zone DEFAULT now(),
    harga numeric(100,0)
);
    DROP TABLE public.m_obat;
       public         heap    postgres    false    226            �            1259    16689 	   m_wilayah    TABLE     =  CREATE TABLE public.m_wilayah (
    m_wilayah_id numeric(20,0) DEFAULT public.get_new_m_wilayah_id() NOT NULL,
    nama character varying(100) NOT NULL,
    active numeric(1,0) DEFAULT 1 NOT NULL,
    create_date timestamp without time zone DEFAULT now(),
    update_date timestamp without time zone DEFAULT now()
);
    DROP TABLE public.m_wilayah;
       public         heap    postgres    false    223            �            1259    16574 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         heap    postgres    false            �            1259    16572    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public          postgres    false    203            �           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
          public          postgres    false    202            �            1259    16635    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         heap    postgres    false            �            1259    16731    t_pasien    TABLE     �  CREATE TABLE public.t_pasien (
    t_pasien_id numeric(20,0) DEFAULT public.get_new_t_pasien_id() NOT NULL,
    nama character varying(50) NOT NULL,
    alamat text,
    no_hp numeric(50,0),
    email character varying(50),
    active numeric(1,0) DEFAULT 1,
    create_date timestamp without time zone DEFAULT now(),
    update_date timestamp without time zone DEFAULT now(),
    status numeric(1,0) DEFAULT 0,
    id_pendaftaran character varying(100),
    total_harga numeric(100,0)
);
    DROP TABLE public.t_pasien;
       public         heap    postgres    false    216            �            1259    16753    t_pasien_detail    TABLE     �  CREATE TABLE public.t_pasien_detail (
    t_pasien_detail_id numeric(20,0) DEFAULT public.get_new_t_pasien_detail_id() NOT NULL,
    t_pasien_id numeric(20,0) NOT NULL,
    m_obat_id numeric(20,0) NOT NULL,
    qty numeric(50,0),
    harga numeric(100,0),
    total numeric(100,0),
    active numeric(1,0) DEFAULT 1,
    create_date timestamp without time zone DEFAULT now(),
    update_date timestamp without time zone DEFAULT now()
);
 #   DROP TABLE public.t_pasien_detail;
       public         heap    postgres    false    219            �            1259    16624    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone DEFAULT now(),
    updated_at timestamp(0) without time zone DEFAULT now(),
    active numeric(1,0) DEFAULT 1
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16622    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    205            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    204            �            1259    16665 
   users_menu    TABLE     M  CREATE TABLE public.users_menu (
    users_menu_id numeric(20,0) DEFAULT public.get_new_users_menu_id() NOT NULL,
    users_id numeric(20,0) NOT NULL,
    m_menu_detail_id numeric(20,0) NOT NULL,
    _read numeric(1,0) NOT NULL,
    _add numeric(1,0) NOT NULL,
    _update numeric(1,0) NOT NULL,
    _delete numeric(1,0) NOT NULL
);
    DROP TABLE public.users_menu;
       public         heap    postgres    false    218            �
           2604    16577    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            �
           2604    16627    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204    205            ~          0    16699    m_jenis_tindakan 
   TABLE DATA           g   COPY public.m_jenis_tindakan (m_jenis_tindakan_id, active, create_date, update_date, nama) FROM stdin;
    public          postgres    false    212   �Q       y          0    16650    m_menu_detail 
   TABLE DATA           s   COPY public.m_menu_detail (m_menu_detail_id, m_menu_group_id, m_menu_sub_id, nama, route_name, active) FROM stdin;
    public          postgres    false    207    R       z          0    16655    m_menu_group 
   TABLE DATA           I   COPY public.m_menu_group (m_menu_group_id, nama, active, fa) FROM stdin;
    public          postgres    false    208   �R       {          0    16660 
   m_menu_sub 
   TABLE DATA           V   COPY public.m_menu_sub (m_menu_sub_id, m_menu_group_id, nama, active, fa) FROM stdin;
    public          postgres    false    209   S                 0    16721    m_obat 
   TABLE DATA           Z   COPY public.m_obat (m_obat_id, nama, active, create_date, update_date, harga) FROM stdin;
    public          postgres    false    213   �S       }          0    16689 	   m_wilayah 
   TABLE DATA           Y   COPY public.m_wilayah (m_wilayah_id, nama, active, create_date, update_date) FROM stdin;
    public          postgres    false    211   �S       u          0    16574 
   migrations 
   TABLE DATA           :   COPY public.migrations (id, migration, batch) FROM stdin;
    public          postgres    false    203   OT       x          0    16635    password_resets 
   TABLE DATA           C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public          postgres    false    206   �T       �          0    16731    t_pasien 
   TABLE DATA           �   COPY public.t_pasien (t_pasien_id, nama, alamat, no_hp, email, active, create_date, update_date, status, id_pendaftaran, total_harga) FROM stdin;
    public          postgres    false    214   �T       �          0    16753    t_pasien_detail 
   TABLE DATA           �   COPY public.t_pasien_detail (t_pasien_detail_id, t_pasien_id, m_obat_id, qty, harga, total, active, create_date, update_date) FROM stdin;
    public          postgres    false    215   $U       w          0    16624    users 
   TABLE DATA           j   COPY public.users (id, name, email, password, remember_token, created_at, updated_at, active) FROM stdin;
    public          postgres    false    205   �U       |          0    16665 
   users_menu 
   TABLE DATA           n   COPY public.users_menu (users_menu_id, users_id, m_menu_detail_id, _read, _add, _update, _delete) FROM stdin;
    public          postgres    false    210   �V       �           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 6, true);
          public          postgres    false    202            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 3, true);
          public          postgres    false    204            �
           2606    16654     m_menu_detail m_menu_detail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.m_menu_detail
    ADD CONSTRAINT m_menu_detail_pkey PRIMARY KEY (m_menu_detail_id);
 J   ALTER TABLE ONLY public.m_menu_detail DROP CONSTRAINT m_menu_detail_pkey;
       public            postgres    false    207            �
           2606    16659    m_menu_group m_menu_group_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.m_menu_group
    ADD CONSTRAINT m_menu_group_pkey PRIMARY KEY (m_menu_group_id);
 H   ALTER TABLE ONLY public.m_menu_group DROP CONSTRAINT m_menu_group_pkey;
       public            postgres    false    208            �
           2606    16664    m_menu_sub m_menu_sub_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.m_menu_sub
    ADD CONSTRAINT m_menu_sub_pkey PRIMARY KEY (m_menu_sub_id);
 D   ALTER TABLE ONLY public.m_menu_sub DROP CONSTRAINT m_menu_sub_pkey;
       public            postgres    false    209            �
           2606    16725    m_obat m_obat_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.m_obat
    ADD CONSTRAINT m_obat_pkey PRIMARY KEY (m_obat_id);
 <   ALTER TABLE ONLY public.m_obat DROP CONSTRAINT m_obat_pkey;
       public            postgres    false    213            �
           2606    16706 %   m_jenis_tindakan m_tipe_tindakan_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.m_jenis_tindakan
    ADD CONSTRAINT m_tipe_tindakan_pkey PRIMARY KEY (m_jenis_tindakan_id);
 O   ALTER TABLE ONLY public.m_jenis_tindakan DROP CONSTRAINT m_tipe_tindakan_pkey;
       public            postgres    false    212            �
           2606    16693    m_wilayah m_wilayah_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.m_wilayah
    ADD CONSTRAINT m_wilayah_pkey PRIMARY KEY (m_wilayah_id);
 B   ALTER TABLE ONLY public.m_wilayah DROP CONSTRAINT m_wilayah_pkey;
       public            postgres    false    211            �
           2606    16579    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public            postgres    false    203            �
           2606    16757 $   t_pasien_detail t_pasien_detail_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.t_pasien_detail
    ADD CONSTRAINT t_pasien_detail_pkey PRIMARY KEY (t_pasien_detail_id);
 N   ALTER TABLE ONLY public.t_pasien_detail DROP CONSTRAINT t_pasien_detail_pkey;
       public            postgres    false    215            �
           2606    16738    t_pasien t_pasien_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.t_pasien
    ADD CONSTRAINT t_pasien_pkey PRIMARY KEY (t_pasien_id);
 @   ALTER TABLE ONLY public.t_pasien DROP CONSTRAINT t_pasien_pkey;
       public            postgres    false    214            �
           2606    16634    users users_email_unique 
   CONSTRAINT     T   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_email_unique;
       public            postgres    false    205            �
           2606    16669    users_menu users_menu_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.users_menu
    ADD CONSTRAINT users_menu_pkey PRIMARY KEY (users_menu_id);
 D   ALTER TABLE ONLY public.users_menu DROP CONSTRAINT users_menu_pkey;
       public            postgres    false    210            �
           2606    16632    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    205            �
           1259    16641    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public            postgres    false    206            ~   d   x�3�4�4202�5��50Q04�26�22Գ0�4���'�X�X����X�e�j��������)V1�6�Ĝ�<.c}��V��� b!�)��
.�E �\1z\\\ %'�      y   x   x�3�4����"�R ���Y\�i�e4���I�L��,��0Ic��1�Wj^f�BHf^Jbvbg�[��T� U�p�'%�p�	��)�lS΀T�ⴒĢ�<������<�0S���� �/�      z   K   x�3��M,.I-�4�LK�MI,ILJ,N�2�)J�+N�.΄��f�$�d�9}���"��Ģ�=... ��      {   �   x�-�A�@E��Sp�1�&F7n�S��Z��&�|���:�ᥴVwh&17=�ͥ������fJ#��n$�Փ%ሲ猒�ZYb��՛'�a�c��X��&���pE�ZT��A�,����s�?�0N         N   x�3��OJ,QpI�M��4�4202�5��50Q00�25�25G��24�44 .C�f�Ԣ�l�M�h62� ����� ���      }   L   x�3�tJ�K)�K�4�4202�5��50Q04�21�21�37�4���#�e�镘�XT��j��������61�=... T�      u   H   x�3�4204�74�74�7 ����Ē����Ԣ���Ĥ�TNC.3d��(
����R�R�SKZb���� _o      x      x������ � �      �   P   x�3�t��L�LJ�K)�K�0 B�D��Cznbf�^r~.�!�������������������V1d�Ɯ�@����� w�#      �   �   x���K!@�p�^`~:�Yz�s���C�4u��F��Ab1�x� $����n4ݦ��1\f�8��Q��WM9Wfr�Ζ14���L��3���ٹ���_�����d�ڞN�1�������r����W;O����U3�,0��2d���Χ_�r�U�\u���Ő[�b#��]ewD|���<      w   �   x�m��n�@E�3OႭ8ߌ�Ϫ5(�l��a�fB�|ah�y���\����Y�\Nƽ��K�)l�Jww�K	g�̛��X � ܧ� �-֓\�*�����i�����n�vUI�����}!�e�x�erp���ӗ,�/o��$"kU}�C��mx^�i�b���0����%�blJg3�o�DQgF�����/3'��StgSJL�      |   /   x�3�4�4�A.cNc$�	���2�D��2�M�<s �΋���� �
]     